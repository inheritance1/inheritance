/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inheritance;

/**
 *
 * @author tud08
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "white", 0);
        animal.speak();
        animal.walk();
        
        Dog dang = new Dog("Dang", "black&white");
        dang.speak();
        dang.walk();
        
        Dog mome = new Dog("Mome", "white&black");
        mome.speak();
        mome.walk();
        
        Dog to = new Dog("To", "brown");
        to.speak();
        to.walk();
        
        Dog bat = new Dog("Bat", "white&black");
        bat.speak();
        bat.walk();
        
        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();
        
        Duck zom = new Duck("Zom", "Orange");
        zom.speak();
        zom.walk();
        
        Duck gabgab = new Duck("GabGab", "black");
        gabgab.speak();
        gabgab.walk();
        
        System.out.println("Bat is Animal: "+(bat instanceof Animal));
        System.out.println("Mome is Animal: "+(mome instanceof Animal));
        System.out.println("Mome is Animal: "+(mome instanceof Dog));
        System.out.println("GabGab is Animal: "+(gabgab instanceof Duck));
        System.out.println("Zom is Animal: "+(zom instanceof Animal));
        System.out.println("Zom is Duck: "+(zom instanceof Duck));
        System.out.println("Zom is Object: "+(zom instanceof Object));
        System.out.println("Animal is Dog: "+(animal instanceof Dog));
        System.out.println("Animal is Animal: "+(animal instanceof Animal));
        
        Animal ani1 = null;
        Animal ani2 = null;
        ani1 = zom;
        ani2 = zero;
        
        System.out.println("Ani1: Zom is Duck "+(ani1 instanceof Duck));
        
        Animal[] animals = {dang, mome, to, bat, zero, zom, gabgab};
        for(int i=0; i<animals.length; i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
        } 
    }
}
